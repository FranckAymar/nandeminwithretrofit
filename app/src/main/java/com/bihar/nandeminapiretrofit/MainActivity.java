package com.bihar.nandeminapiretrofit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import android.util.Log;
import android.widget.Toast;

import okhttp3.Response;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

    ApiService service;

    Call<ProjetResponse> call;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        service = RetrofitBuilder.createService(ApiService.class);

        fetchProjets();

    }


    private void fetchProjets(){

        call = service.getProjets();

        call.enqueue(new Callback<ProjetResponse>() {

            @Override
            public void onResponse(Call<ProjetResponse> call, retrofit2.Response<ProjetResponse> response) {

                if(response.isSuccessful()){

                    Log.i("Projets", "onResponse: " + response.body().getData().size() );

                    for(int i = 0; i < response.body().getData().size(); i++){

                        Log.i("Projets", "onResponse: " + response.body().getData().get(i).getProjetTitle() );

                    }


                }else {

                    Log.i("Projets", "onResponse: " + "Erreur dans la recuperation des projets" );

                }
            }

            @Override
            public void onFailure(Call<ProjetResponse> call, Throwable t) {
                Log.e("Projets", "onResponse: " + t.getMessage() );

                Toast.makeText(MainActivity.this, "Vérifier votre connexion à internet", Toast.LENGTH_SHORT).show();
            }

        });
    }

}

