package com.bihar.nandeminapiretrofit;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("projets")
    Call<ProjetResponse> getProjets();



}
