package com.bihar.nandeminapiretrofit;

import com.squareup.moshi.Json;

public class ProjetModel {

    @Json(name = "id")
    private Long id ;
    @Json(name = "title")
    private String projetTitle ;
    @Json(name = "fonds")
    private Integer sommeTotale ;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProjetTitle() {
        return projetTitle;
    }

    public void setProjetTitle(String projetTitle) {
        this.projetTitle = projetTitle;
    }

    public Integer getSommeTotale() {
        return sommeTotale;
    }

    public void setSommeTotale(Integer sommeTotale) {
        this.sommeTotale = sommeTotale;
    }

}
